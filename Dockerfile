FROM drupal:9.4.8

RUN composer require aws/aws-sdk-php:3.173.5 && composer require drupal/phpmailer_smtp && composer require firebase/php-jwt
